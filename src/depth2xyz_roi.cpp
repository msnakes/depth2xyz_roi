#include <ros/ros.h>
#include <sensor_msgs/CameraInfo.h>
#include <sensor_msgs/Image.h>
#include <depth2xyz_roi/DepthToXYZ.h>
#include <geometry_msgs/Point32.h>
std::string default_depth_topic = "/camera/aligned_depth_to_color/image_raw";
std::string default_depth_camera_info = "/camera/aligned_depth_to_color/camera_info";
bool callback(depth2xyz_roi::DepthToXYZ::Request &req,
                depth2xyz_roi::DepthToXYZ::Response &res){
  std::string depth_image_topic, depth_camera_info;
  if(req.depth_topic.empty()) depth_image_topic = default_depth_topic;
  else depth_image_topic = req.depth_topic;
  if(req.camera_info_topic.empty()) depth_camera_info = default_depth_camera_info;
  else depth_camera_info = req.camera_info_topic;
  sensor_msgs::Image::ConstPtr depth_image 
     = ros::topic::waitForMessage<sensor_msgs::Image>(depth_image_topic,ros::Duration(3));
  if(depth_image!=NULL){
    int start[2];
    int end[2];
    if(req.start[0]>req.end[0]){
      start[0] = req.end[0];
      end[0] = req.start[0];
    }else{
      start[0] = req.start[0];
      end[0] = req.end[0];
    }
    if(req.start[1]>req.end[1]){
      start[1] = req.end[1];
      end[1] = req.start[1];
    }else{
      start[1] = req.start[1];
      end[1] = req.end[1];
    }
    sensor_msgs::CameraInfo::ConstPtr camera_info 
     = ros::topic::waitForMessage<sensor_msgs::CameraInfo>(depth_camera_info,ros::Duration(3));
    if(camera_info!=NULL){
      if(end[0]>depth_image->width) end[0] = depth_image->width;
      if(end[1]>depth_image->height) end[1] = depth_image->height; 
      float valid_num = 0.0;
      float dists = 0.0;
      for (int y = start[1]; y <= end[1]; y++)
      {
        for (int x = start[0]; x <= end[0]; x++)
        {
          geometry_msgs::Point32 point;
          int idx = y * depth_image->step + x * 2; // 每个像素占用2字节
          point.z = ((depth_image->data[idx + 1] << 8) | depth_image->data[idx])*0.001;//深度单位为毫米，转换为米
          std::cout<<"z: "<<point.z<<std::endl;
          if(std::isfinite(point.z)){
            point.x = (x - camera_info->K.at(2)) * point.z / camera_info->K.at(0);
            point.y = (y - camera_info->K.at(5)) * point.z / camera_info->K.at(4);
          }else{
            point.z = 0.0;
          }
          res.position.push_back(point);
          res.success = true;
        }
      }
    }else res.err = "camera info topic: <<< "+ depth_camera_info+" >>> is not find";
  }else res.err = "depth image topic: <<< "+ depth_image_topic +" >>> is not find";;
  return true;
}

int main(int argc, char * argv[])
{
  ros::init(argc, argv, "depth2xyz_roi");
  ros::NodeHandle nh("~");
  ros::NodeHandle n;
  ros::ServiceServer service = n.advertiseService("get_depth_roi_pos", callback);
  nh.param<std::string>("depth_topic",default_depth_topic,default_depth_topic);
  nh.param<std::string>("camera_info_topic",default_depth_camera_info,default_depth_camera_info);
  ros::spin();
  ros::shutdown();
  return 0;
}
